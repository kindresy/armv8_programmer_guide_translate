# 章节3 ARMv8基础

在ARMv8中，执行发生在四种异常等级其中一个等级上. 在AArch64里，异常等级决定了权限等级，和ARMv7定义的特权等级是类似的。异常等级决定了特权等级，所以，执行在异常等级ELn对应着特权等级PLn。与ARMv7相似，异常等级ELn的n值比另一个异常等级大，说明它在更高的异常等级上。异常等级的数字值小于另一个异常等级，被认为处于一个更低的异常等级上。(ELn, n值越大，异常等级越高，权限也越高)

异常级别提供了适用于ARMv8体系结构的所有操作状态的软件执行特权的逻辑分离。这就类似于计算机科学里通用的分层级保护域的概念(hierarchical protection domains)

如下是软件运行在各个异常等级的典型案例:

| 异常等级 | 运行案例                                                  |
| -------- | --------------------------------------------------------- |
| EL0      | Normal user applications                                  |
| EL1      | Operating system kernel typically described as privileged |
| EL2      | Hypervisor                                                |
| EL3      | Low-level firmware, including the Secure Monitor          |

**Figure 3-1 Exception levels**

![image-20231012150938047](./章节3 ARMv8基础.assets/image-20231012150938047.png)

一般来说，一个软件，例如一个应用程序，一个OS内核，或者一个虚拟机管理程序supervisor，占据单独的一个异常等级。这个规则的一个例外是核内虚拟机管理程序(in-kernel hypervisors)例如KVM，其同时在EL2和EL1上运行。

ARMv8-A提供了两种安全状态，Secure和Non-secure。非安全状态也被称为Normal World。这使得一个操作系统能够在同样的硬件上**平行地**运行可信的OS(trusted OS)，并提供了针对特定软件攻击和硬件攻击的保护。ARM TrustZone技术使系统能够被划分为Normal world和Seucre world。与ARMv7架构一样，Secure Monitor充当了在Normal world和Secure world之间移动的通道。

**Figure 3-2 ARMv8 Exception levels in the Normal and Secure worlds**

![image-20231012153050234](./章节3 ARMv8基础.assets/image-20231012153050234.png)

ARMv8-A也提供了对虚拟化的支持，包括在Normal World的情况下。这意味着hypervisor，或者虚拟机器管理器VMM的代码能够在系统上运行并且托管多个guest操作系统。每一个guest 操作系统本质上都是运行在一个虚拟机器上，每一个OS察觉不到它正在和其他guest操作系统**分时运行**

Normal World(对应Non-secure state)有一下特权组件：

**Guest OS kernels**

​	这种kernels包括运行在Non-secure的EL1下的linux或windows。当运行在hypervisor下时，rich os kernels能够被作为一个guest或时托管在hypervisor模式下

**hypervisor**

​	它在EL2上运行，而EL2总是不安全的。当hypervisor存在并启用时，它会为rich os内核提供虚拟化服务

Secure world有如下特权组件:

**Secure firmware**

​	在一个应用程序处理器上，这个固件必须是boot阶段运行的第一件事，它提供几个服务，包括平台初始化，trusted OS的安装，以及secure monitor 调用的路由

**Trusted OS**

​	Trusted OS给Normal World提供了安全服务并给执行中的安全或可信应用提供了一个运行时环境

ARMv8体系结构中的**Seucre Monitor处在一个最高的异常等级下并且它的权限比其他任意一个等级都要高**。这就提供了一个软件权限的逻辑模型

Figure 3-2显示了EL2的安全模式是不可用的

## 3.1 执行状态

ARMv8体系结构定义了两种执行状态，AArch64和AArch32。每种状态分别用于描述使用64bit宽通用寄存器还是32bit宽通用寄存器的执行。虽然ARMv8的AArch32保留了ARMv7的特权级定义，但在AArch64中，特别级别由异常级别确定。因此，执行在ELn就对应于特权PLn。

处于AArch64状态下时，处理器执行A64指令集。当处于AArch32状态时，处理器既能够执行A32(被叫做ARM早期的架构版本)也能够执行T32(Thumb)指令集

下面的图表显示了AArch64和AArch32下的异常等级的组织:

**Figure 3-3 Exception levels in AArch64**

![image-20231012204000702](./章节3 ARMv8基础.assets/image-20231012204000702.png)

**Figure 3-4 Exception levels in AArch32**

![image-20231012204020450](./章节3 ARMv8基础.assets/image-20231012204020450.png)

在AArch32状态下，Trusted OS的软件执行在Secure EL3，在AArch64状态下它主要运行在Secure EL1

## 3.2 改变异常等级

在ARMv7架构下，在特权软件的控制下或者自动的产生异常，能够改变处理器的模式。当异常产生时，core将保存当前的执行状态和返回地址，进入所需模式，并且有可能会禁用硬件中断。

下面的表格中包含了摘要:

- 应用程序运行在更低等级的特权级PL0，也就是ARM架构过去的非特权模式。
- 操作系统运行在PL1
- Hypervisor系统和虚拟化扩展都运行在PL2
- 作为secure和non-secure worlds的通路(桥接)的 Secure monitor也运行在PL1
- SVC，当处理器进入reset（若默认启动状态为AArch32，则处理器启动后的默认异常等级为SVC），或者执行SVC指令时进入

[Table 3-1 ARMv7 processor modes]()

![image-20231012204920149](./章节3 ARMv8基础.assets/image-20231012204920149.png)

（注:  从中也可以发现，只有HYP 模式处于PL2 异常等级（Non-secure only），只有USR模式处于PL0等级，其余模式都处于PL1）

**Figure 3-5 ARMv7 privilege levels**

![image-20231012205012965](C:\Users\EDZ\Desktop\arm文档编写\章节3 ARMv8基础.assets\image-20231012205012965.png)

在AArch64下，处理器模式被映射到异常等级，就像下图Figure 3-6描述的。与在ARMv7(AArch32)下一样，当发生异常时，处理器更改为支持异常处理的异常等级(或者说模式)。

**Figure 3-6 AArch32 processor modes**

![image-20231012205438051](./章节3 ARMv8基础.assets/image-20231012205438051.png)

异常等级之间的转移遵循以下规则:

- 转移到一个更高的异常等级，例如从EL0到EL1，也意味着软件执行特权级的提升
- 一个异常的产生无法将当前异常等级转移到更低级的异常等级
- EL0等级下没有异常处理，异常必须被带到一个更高异常等级去处理
- 异常会造成程序执行流被更改。从一个与异常相关的被定义的向量表，异常处理程序执行在高于EL0的异常级别，异常包括：
  - 例如IRQ和FIQ这类中断
  - 内存系统中止
  - 未定义的指令
  - 系统调用，这允许非特权软件去执行系统调用进入操作系统
  - Secure monitor 和 hypervisor陷阱
- 通过执行**ERET指令**来结束异常处理并返回上一个异常等级
- **从一个异常返回能够留在相同异常等级也可以进入一个更低的异常等级。不可以进入一个更高的异常等级。**
- 安全状态的切换确实会跟着异常等级的切换而切换，从EL3返回到一个Non-secure state是一个例外。参考 Switching between Secure and Non-secure state on  page 17-8.



## 3.3 改变异常执行状态

有几种情况下你必须切换系统的执行状态，例如，可能有:如果你正在运行一个64bit操作系统，并且想要运行一个32bit的应用程序在EL0。为了做到这一点，系统必须切换至AArch32.

当应用程序已经执行完成或者执行流返回到OS，系统能够切换回AArch64。 Figure 3-7 显示你无法反过来做(从AArch64返回AArch32)，一个在AArch32状态运行的操作系统无法托管一个64位的应用程序

为了在同一异常等级下实现状态间的切换，必须切换到一个更高的异常等级下然后再返回到原来的异常等级。例如，你也许在一个64bit的OS上运行着32-bit和64-bit的应用程序，在这个例子中，32bit的应用程序能够执行一个Supervisor Call(SVC) 指令，或者接收一个中断，引起切换到EL1和AArch64状态（参考Exception handling instructions on page 6-21）。操作系统可以在AArch64状态下执行一个任务切换并返回到EL0。实际上来说，这意味着你无法使用一个32-bit和64-bit混合的程序，因为没有直接的在这两者之间相互调用的方式

你只能通过改变异常等级来切换执行状态。发生异常也许会从 AArch32切换到AArch64。从一个异常返回也许会从AArch64切换到AArch32。

运行在EL3的代码无法通过异常到达一个更高的异常等级，所以无法改变执行状态，除非通过reset

以下是在AArch64和AArch32执行状态之间切换时的一些要点摘要:

- AArch64和AArch32执行状态的异常级别大致相似，但secure和non-secure操作之间存在一些差异。异常产生时处理器所处的执行状态能够限制其他执行状态可用的异常级别(why?)
- 切换至AArch32需要从高异常级别切换至低异常级别。这是通过执行ERET指令退出异常处理的结果，参见**Exception  handling instructions on page 6-21.**
- 切换至AArch64需要从低异常级别切换至高异常级别。这种异常可能是指令执行或外部信号引起的。
- 当发生异常或从异常返回时，如果异常级别保持不变，则执行状态不能更改
- 当ARMv8处理器在特定异常级别的AArch32执行状态下运行时，它对达到该异常级别的异常使用与ARMv7中相同的异常模型。
  在AArch64执行状态下，它使用第10章AArch64异常处理中介绍的异常处理模型

因此在secure monitor, hypervisor或者os级别上执行两种状态的交互。一个执行在AArch64状态的hypervisor或者os能够支持AArch32运行在低特权级。这意味着一个运行在AArch64上的操作系统可以托管AArch32和AArch64的应用程序。类似的，一个运行在AArch64上的hypervisor可以托管AArch32和AArch64的guest操作系统。而然，一个32bit操作系统无法支持64bit应用程序且一个32bit的hypervisor无法支持64bit的guest操作系统。

**Figure 3-7 Moving between AArch32 and AArch64**

![image-20231013153039491](./章节3 ARMv8基础.assets/image-20231013153039491.png)

对于最高实现的异常级别(Cortex-A53和Cortex-A57处理器上的EL3)，在接受异常时用于每个异常级别的执行状态是固定的。
只能通过重置处理器来更改异常级别。
对于EL2和EL1，它由System registers控制，参见**System registers**



# 总结

AArch64兼容AArch32程序，也就是说AArch32的程序可以跑在支持AArch64 ISA的硬件上但反过来无法实现。

AArch64中异常等级的概念类似与AArch32中特权等级，且ELn越大意味着PLn越大

改变异常等级一般通过触发异常，外部中断，SVC等

执行状态的改变一般通过异常,可能是指令执行或外部信号

为了在同一异常等级下实现状态间的切换，必须切换到一个更高的异常等级下然后再返回到原来的异常等级

当发生异常或从异常返回时，如果异常级别保持不变，则执行状态不能更改

































































