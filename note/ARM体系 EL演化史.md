# ARM体系 EL演化史

arm最开始的特权级别两种：一个用户态，是unprivilege level，一个系统态，是privilege level。用户态用于执行常用的软件，而系统态用于运行操作系统。unprivilege level的 特权等级为 PL0，privilege level的特权级别为PL1

这个时候arm的软件执行也被分为7中：usr，fiq，irq，svc，abt，und，sys。其中usr位于unprivilege level下，其他六个都位于privilege level下

usr和sys之外的五种模式，被称为异常模式，设计这么多的异常模式是为了更好的，分而治之地处理异常，可以使模式切换过程（也是异常处理过程）更为高效、便利。

![image-20231018192739011](C:\Users\EDZ\AppData\Roaming\Typora\typora-user-images\image-20231018192739011.png)

- usr, user mode
- fiq, 处理FIQ中断模式
- irq, 处理IRQ中断模式
- svc, supervisor mode
- abt, abort mode
- und, undefined mode
- sys, system mode

![image-20231018192926440](C:\Users\EDZ\AppData\Roaming\Typora\typora-user-images\image-20231018192926440.png)



考虑到ARMv7-a中可选的3个扩展:

1. **security扩展**，为支持建立可信赖的执行环境（trust execution environment, TEE）而引入等扩展。
2. **虚拟化扩展**，这是arm体系志在进军高阶服务器市场而产生的一个扩展
3. lpae，这个相比而言就没有前两者那么清楚，事实上与其说lpae影响到了EL/PL，不如说EL/PL影响到了lpae更加合适。

**security扩展**要求所有这些模式都要有另一种额外的执行状态，所以PL0和PL1就又分为了security版本和non-security版本，以支持在security状态下和non-security状态下都可以运行os+software

为了让security状态和non-security状态可以相互切换，应该还有一个bridge的地方，因此，引入了monitor mode

这个monitor mode显然应该处于security状态下，才能操作一些security下的资源，并且应该处于特权模式。所以monitor mode应该处于security的PL1模式

自此，特权模式和security状态的格局应该是如下图这个样子：

![image-20231018194927265](C:\Users\EDZ\Desktop\translate_arm\armv8_programmer_guide_translate\note\ARM体系 EL演化史.assets\image-20231018194927265.png)

**虚拟化扩展**的最大的好处是，可以在一个物理硬件平台上虚拟处多个虚拟的硬件平台。

由于security状态本身非常依赖于硬件，如果这个硬件是在物理硬件上虚拟出来的，那还了得？故而security不能支持虚拟化。那么现在看来，只能在non-security世界实现虚拟化了。

为了支持虚拟化，需要做哪些模式的改变呢？现在，在non-security世界，我们有PL0运行usr模式的代码，有PL1运行系统的代码。显然，这是一个硬件系统应当有的基本的特权分层。如果需要支持虚拟化，最简单的方法就是加一个PL2，其特权级别还高于PL1。这也是arm的做法，并且特意在这里，给这种实现虚拟化支持的模式起了一个hyp的名称，hypervisor的缩写。

![image-20231018194405952](C:\Users\EDZ\AppData\Roaming\Typora\typora-user-images\image-20231018194405952.png)



armv8最大的变化就是加入了64位支持。在PL上，armv8提出了新的EL的概念。在纯64位环境下，这个EL相对于我们上图其实变化不是很大，主要是将PL0对应EL0，PL2对应EL2，除了security下monitor之外的PL1对应EL1，而将security下的monitor模式单独抽取出来，放到一个EL3上。至此，我们有了EL0到EL3，并且由于EL2是虚拟化引入的，所以只能存在于non-security世界，而EL3是由security引入到，故而只能存在于security世界。

> 可能ARMv8-a的设计者觉得之前的设计有些啰嗦，就把processor mode的概念去掉（或者说淡化）了，取而代之的是4个固定的Exception level，简称EL0-EL3。同时，也淡化了privilege level的概念。Exception level本身就已经包好了privilege的信息，即ELn的privilege随着n的增大而增大。类似地，可以将EL0归属于non-privilege level，EL1/2/3属于privilege level。
>
> EL0是user模式
> EL1是内核（EL0的user和EL1是6种模式合起来是32位ARM的7种工作模式）
> EL2是HYP(虚拟化扩展)
> EL3是Monitor（用于安全/非安全世界的切换）

![image-20231018194532994](C:\Users\EDZ\AppData\Roaming\Typora\typora-user-images\image-20231018194532994.png)

然而，引入64位支持后，还有一个需要额外考量的地方：平台的32位/64位属性。我们都知道，64位系统是可以运行32位软件的，反之则不行。在这里同样，如果高特权的EL实现为64位模式，则其下的特权模式可以选择实现为32位或者64位。然而，如果高特权模式的EL实现为32位，则其下的特权模式只能实现为32位。



> ARMv8-a Exception level有关的说明如下：
>
> 1）首先需要注意的是，AArch64中，已经没有User、SVC、ABT等处理器模式的概念，但ARMv8需要向前兼容，在AArch32中，就把这些处理器模式map到了4个Exception level。
>
> 2）Application位于特权等级最低的EL0，Guest OS（Linux kernel、window等）位于EL1，提供虚拟化支持的Hypervisor位于EL2（可以不实现），提供Security支持的Seurity Monitor位于EL3（可以不实现）。
>
> 3）只有在异常发生时（或者异常处理返回时），才能切换Exception level（这也是Exception level的命名原因，为了处理异常）。当异常发生时，有两种选择，停留在当前的EL，或者跳转到更高的EL，EL不能降级。同样，异常处理返回时，也有两种选择，停留在当前EL，或者调到更低的EL



> ARMv8-a的security模型基本沿用了ARMv7 security extension的思路，主要目的保护一些安全应用的数据，例如支付等。它不同于privilege level等软件逻辑上的保护，而是一种物理上的区隔，即不同security状态下，可以访问的物理内存是不同的。
>
> ARMv8-a架构有两个security state（参考上面图片），Security和non-Security。主要的功效是物理地址的区隔，以及一些system control寄存器的访问控制：
>
> 在Security状态下，处理器可以访问所有的Secure physical address space以及Non-secure physical address space；
>
> 在Non-security状态下，只能访问Non-secure physical address space，且不能访问Secure system control resources。