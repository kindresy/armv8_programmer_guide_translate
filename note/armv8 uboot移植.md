# 基本符号定义

```
Board               -> fpga
Vendor              ->  tsingmicro
Machine(SoC)    	->  dt53
Arch                -> arm
CPU                 ->  armv8
```

# 目录结构以及Kconfig确定

1. 创建board/tsingmicro/dt53目录，并提供Kconfig和Makefile

![image-20231020125904695](C:\Users\EDZ\AppData\Roaming\Typora\typora-user-images\image-20231020125904695.png)

2. 在arch/arm/Kconfig添加DT53 FPGA的配置菜单

```
config ARCH_TS_DT53
	bool "Tsingmicro dt53 family"
	select ARCH_TS	
	select ARM64
	select CLK
	select DM
	select DM_ETH if NET
	select DM_MMC if MMC
	select DM_SERIAL
	select DM_SPI if SPI
	select DM_SPI_FLASH if DM_SPI
	select DM_USB if USB
	select FIRMWARE
	select OF_CONTROL
	# select OF_LIVE
	select SPL_BOARD_INIT if SPL
	select SPL_CLK if SPL
	select SPL_DM_SPI if SPI
	select SPL_DM_SPI_FLASH if SPL_DM_SPI
	select SPL_DM_MAILBOX if SPL
	select SPL_FIRMWARE if SPL
	select SPL_SEPARATE_BSS if SPL
	select SUPPORT_SPL
	#
	imply BOARD_LATE_INIT
	imply CMD_DM
	imply ENV_VARS_UBOOT_RUNTIME_CONFIG
	imply FAT_WRITE
	imply MP
	imply DM_USB_GADGET	
	
source "board/tsingmicro/Kconfig"
source "board/tsingmicro/dt53/Kconfig"
```

此处会select ARM64，此时arch、board、CPU等结构已经确定。同时，我们select了SUPPORT_SPL和SPL两个配置项，表明该版型将会使用SPL功能。

3. 根据符号定义在board/tsingmicro/dt53/Kconfig添加配置

```
if ARCH_TS_DT53

config SYS_BOARD
       default "fpga"

config SYS_VENDOR
       default "tsingmicro"

config SYS_SOC
       default "dt53"

config SYS_CONFIG_NAME
       default "ts_dt53"

endif
```

4. 创建该板子有关的配置头文件

u-boot的配置有两种方式：

一种是autoconf的方式，通过menuconfig生成.config，然后再转换为conf.h，类似于kernel的配置文件；

另一种是通过版型有关的头文件（如上面的include/configs/ts_dt53.h）。

```
/* SPDX-License-Identifier: GPL-2.0+ */
/*
 * Configuration for Tsingmicro TX511
 * (C) Copyright 2021 Tsingmicro, Inc.
 *
 */

#ifndef __TSINGMICRO_DT53_H
#define __TSINGMICRO_DT53_H
#include <linux/sizes.h>

//#define DEBUG

/*
 * TAGS
 */

#define CONFIG_CMDLINE_TAG
//#define CONFIG_SETUP_MEMORY_TAGS
//#define CONFIG_INITRD_TAG
//#define CONFIG_REVISION_TAG
//#define CONFIG_SERIAL_TAG


#define CONFIG_REMAKE_ELF

#define CONFIG_SYS_INIT_SP_ADDR		CONFIG_SYS_TEXT_BASE

```

5. 使用menuconfig，生成.config，并保存为ts_dt53_defconfig

```
cd uboot/
make menuconfig
```

配置Architecture和Target：

Architecture select (ARM architecture) --->

ARM architecture --->
  Target select (dt53) --->

cp .config configs/bubblegum_defconfig

6. ##### 在“include/configs/ts_dt53.h”添加必要的配置项

\#define CONFIG_SPL_TEXT_BASE 0xe406b200

\#define CONFIG_SPL_MAX_SIZE (1024 * 20)

\#define CONFIG_SPL_STACK 0xe407f000

7. ##### 在“board/tsingmicro/dt53/”中添加board有关的C文件