# 1 ATF 启动流程

TF-A，arm trusted fireware是ARM提供的一套通用的软件架构+特定于平台的实现代码。

在使用TrustZone技术的嵌入式设备当中，无法避免的需要解决从非安全侧切换到安全侧的问题，而为了完成这个切换，需要一个专用的进行非安全上下文和安全上下文切换的固件，这个固件一般我们称为arm trust firmware。

ATF实现了以下arm接口标准：

TF-A implements Arm interface standards, including:

- [Power State Coordination Interface (PSCI)](http://infocenter.arm.com/help/topic/com.arm.doc.den0022d/Power_State_Coordination_Interface_PDD_v1_1_DEN0022D.pdf)
- [Trusted Board Boot Requirements CLIENT (TBBR-CLIENT)](https://developer.arm.com/docs/den0006/latest/trusted-board-boot-requirements-client-tbbr-client-armv8-a)
- [SMC Calling Convention](http://infocenter.arm.com/help/topic/com.arm.doc.den0028b/ARM_DEN0028B_SMC_Calling_Convention.pdf)
- [System Control and Management Interface (SCMI)](http://infocenter.arm.com/help/topic/com.arm.doc.den0056a/DEN0056A_System_Control_and_Management_Interface.pdf)
- [Software Delegated Exception Interface (SDEI)](http://infocenter.arm.com/help/topic/com.arm.doc.den0054a/ARM_DEN0054A_Software_Delegated_Exception_Interface.pdf)

# 2 ATF 作用

主要有两个作用：

- 启动

- runtime service，提供电源管理、core的hotplug和idle管理等。与启动不同，这部分代码在linux启动以后仍然工作，上层通过SMC(SMC是一条ARM指令，Generate exception targeting exception level3)陷入到runtime service里获取相应服务。



# 3 ATF BL执行顺序

**ATF框架里，包含BL1,BL2,BL31，不包含BL32,BL33**

- Boot Loader stage 1 (BL1) AP Trusted ROM: 用于实现bootrom
- Boot Loader stage 2 (BL2) Trusted Boot Firmware，二级loader
- Boot Loader stage 3-1 (BL31) EL3 Runtime Software， 提供PSCI(电源管理等)、secure和nosecure切换等运行时服务
- Boot Loader stage 3-2 (BL32) Secure-EL1 Payload (optional)， 第三方的secure OS，如TEE等
- Boot Loader stage 3-3 (BL33) Non-trusted Firmware，一般是uboot

BL1->BL2->BL31/32->BL33
也不绝对，有些系统里没有BL1和BL2； 有些没有BL2。

TF-A提供另外一种启动代码流程：使用BL3作为reset entrypoint，即复位后执行bl3_entrypoint的代码

Boot Sequence

=============

Bootrom --> BL31 --> BL33(u-boot) --> Linux kernel

# 4 ATF 代码结构

TF-A code is logically divided between the three boot loader stages mentioned in the previous sections. The code is
also divided into the following categories (present as directories in the source code):

- Platform specific. Choice of architecture specific code depends upon the platform.
- Common code. This is platform and architecture agnostic code.
- Library code. This code comprises of functionality commonly used by all other code. The PSCI implementation and other EL3 runtime frameworks reside as Library components.
- Stage specific. Code specific to a boot stage.
- Drivers.
- Services. EL3 runtime services (eg: SPD). Specific SPD services reside in the services/spd directory (e.g.
  services/spd/tspd).

Each boot loader stage uses code from one or more of the above mentioned categories. Based upon the above, the
code layout looks like this:

| Directory | Used by BL1? | Used by BL2? | Used by BL31? |
| :-------- | :----------- | :----------- | :------------ |
| bl1       | Yes          | No           | No            |
| bl2       | No           | Yes          | No            |
| bl31      | No           | No           | Yes           |
| plat      | Yes          | Yes          | Yes           |
| drivers   | Yes          | No           | Yes           |
| common    | Yes          | Yes          | Yes           |
| lib       | Yes          | Yes          | Yes           |
| services  | No           | No           | Yes           |

# 5 ATF 组成

![image-20231018211218172](C:\Users\EDZ\AppData\Roaming\Typora\typora-user-images\image-20231018211218172.png)

# 6 ATF 启动过程概览

![image-20231018211314838](C:\Users\EDZ\AppData\Roaming\Typora\typora-user-images\image-20231018211314838.png)

# 7 ATF BL1执行流程

![ATF bl1执行流程](C:\Users\EDZ\Desktop\translate_arm\armv8_programmer_guide_translate\note\ATF 启动流程.assets\ATF bl1执行流程.png)

# 8 BL1 源码阅读

链接文件bl1.ld.S, 入口为bl1_entrypoint.S

```assembly
MEMORY {
 ROM (rx): ORIGIN = BL1_RO_BASE, LENGTH = BL1_RO_LIMIT - BL1_RO_BASE	//平台相关的ROM启动
 RAM (rwx): ORIGIN = BL1_RW_BASE, LENGTH = BL1_RW_LIMIT - BL1_RW_BASE	//平台相关的RAM地址
}

SECTIONS

{
 . = BL1_RO_BASE;	
 ASSERT(. == ALIGN(PAGE_SIZE),	
 "BL1_RO_BASE address is not aligned on a page boundary.")

#if SEPARATE_CODE_AND_RODATA
 .text . : {
 __TEXT_START__ = .;
 *bl1_entrypoint.o(.text*)
 *(.text*)
 *(.vectors)
 . = ALIGN(PAGE_SIZE);
 __TEXT_END__ = .;
 } >ROM
```

bl1_entrypoint.S

```assembly
.globl bl1_entrypoint

/* -----------------------------------------------------	//CPU从warm或cold reset释放时bl1_entrypoint是可信固件代码的
 * bl1_entrypoint() is the entry point into the trusted
 * firmware code when a cpu is released from warm or
 * cold reset.
 * -----------------------------------------------------
 */

func bl1_entrypoint
 /* ---------------------------------------------------------------------
 * If the reset address is programmable then bl1_entrypoint() is
 * executed only on the cold boot path. Therefore, we can skip the warm
 * boot mailbox mechanism.
 * 
 * 如果重置地址是可编程的，那么bl1_entrypoint()只在冷启动路径上执行，因此我们可以
 * 跳过热启动mailbox机制
 * ---------------------------------------------------------------------
 */

 /*
 el3_entrypoint_common是bl1和bl31公用的一个macro，做基本的系统初始化，sp初始化
 */
 el3_entrypoint_common \
 _init_sctlr=1 \
 _warm_boot_mailbox=!PROGRAMMABLE_RESET_ADDRESS \
 _secondary_cold_boot=!COLD_BOOT_SINGLE_CPU \
 _init_memory=1 \
 _init_c_runtime=1 \
 _exception_vectors=bl1_exceptions

/* --------------------------------------------------------------------
 * Perform BL1 setup			平台BL1设置
 * --------------------------------------------------------------------
 */
 bl bl1_setup

/* --------------------------------------------------------------------
 * Enable pointer authentication	启用指针身份验证
 * --------------------------------------------------------------------
 */
#if ENABLE_PAUTH
 mrs x0, sctlr_el3
 orr x0, x0, #SCTLR_EnIA_BIT
 msr sctlr_el3, x0
 isb
#endif /* ENABLE_PAUTH */

/* --------------------------------------------------------------------
 * Initialize platform and jump to our c-entry point
 * for this type of reset.
 * 初始化平台并跳转到我们的针对这个resset类型的c入口指针
 * --------------------------------------------------------------------
 */
 bl bl1_main

/* --------------------------------------------------------------------
 * Disable pointer authentication before jumping to BL31 or that will
 * cause an authentication failure during the early platform init.
 * --------------------------------------------------------------------
 */
#if ENABLE_PAUTH
 mrs x0, sctlr_el3
 bic x0, x0, #SCTLR_EnIA_BIT
 msr sctlr_el3, x0
 isb
#endif /* ENABLE_PAUTH */

/* --------------------------------------------------
 * Do the transition to next boot image.
 * 执行跳转到下一个boot镜像
 * --------------------------------------------------
 */
 b el3_exit
endfunc bl1_entrypoint
```

el3_entrypoint_common

include\arch\aarch64\el3_common_macro.S

```asm
/* -----------------------------------------------------------------------------
 * This is the super set of actions that need to be performed during a cold boot
 * or a warm boot in EL3. This code is shared by BL1 and BL31.
 *
 * This macro will always perform reset handling, architectural initialisations
 * and stack setup. The rest of the actions are optional because they might not
 * be needed, depending on the context in which this macro is called. This is
 * why this macro is parameterised ; each parameter allows to enable/disable
 * some actions.
 *
 * 这是在EL3中冷启动或热启动期间需要执行的一组超级操作。此代码由BL1和BL31共享。
 * 这个宏将始终执行重置处理、架构初始化和堆栈设置。其余的操作是可选的，因为它们可能不需要，
 * 这取决于调用该宏的上下文。这就是为什么这个宏是参数化的;每个参数允许启用/禁用某些操作。
 *
 * _init_sctlr:
 * Whether the macro needs to initialise SCTLR_EL3, including configuring
 * the endianness of data accesses.
 * 是否初始化SCTLR_EL3，包括配置数据访问的大小端
 *
 * _warm_boot_mailbox:
 * Whether the macro needs to detect the type of boot (cold/warm). The
 * detection is based on the platform entrypoint address : if it is zero
 * then it is a cold boot, otherwise it is a warm boot. In the latter case,
 * this macro jumps on the platform entrypoint address.
 * 是否这个宏需要探测boot类型(cold/warm). 探测基于平台入口地址: 如果是0那么是从冷启动
 * 否则是热启动，在后面一种情况下，这个宏跳转到平台入口点地址
 *
 * _secondary_cold_boot:
 * Whether the macro needs to identify the CPU that is calling it: primary
 * CPU or secondary CPU. The primary CPU will be allowed to carry on with
 * the platform initialisations, while the secondaries will be put in a
 * platform-specific state in the meantime.
 * 宏是否需要识别调用它的CPU:主CPU或辅助CPU。
 * 主CPU将被允许进行平台初始化
 * 而辅助CPU将在此期间处于特定于平台的状态。
 *
 * If the caller knows this macro will only be called by the primary CPU
 * then this parameter can be defined to 0 to skip this step.
 *
 * _init_memory:
 * Whether the macro needs to initialise the memory.
 *
 * _init_c_runtime:
 * Whether the macro needs to initialise the C runtime environment.
 *
 * _exception_vectors:
 * Address of the exception vectors to program in the VBAR_EL3 register.
 * -----------------------------------------------------------------------------
 */
 .macro el3_entrypoint_common \
 _init_sctlr, _warm_boot_mailbox, _secondary_cold_boot, \
 _init_memory, _init_c_runtime, _exception_vectors

.if \_init_sctlr
 /* -------------------------------------------------------------
 * This is the initialisation of SCTLR_EL3 and so must ensure
 * that all fields are explicitly set rather than relying on hw.
 * Some fields reset to an IMPLEMENTATION DEFINED value and
 * others are architecturally UNKNOWN on reset.
 *
 * SCTLR.EE: Set the CPU endianness before doing anything that
 * might involve memory reads or writes. Set to zero to select
 * Little Endian. 设置为little endian
 *
 * SCTLR_EL3.WXN: For the EL3 translation regime, this field can
 * force all memory regions that are writeable to be treated as
 * XN (Execute-never). Set to zero so that this control has no
 * effect on memory access permissions. 允许写的区域是否可执行，此处设置为可执行
 *
 * SCTLR_EL3.SA: Set to zero to disable Stack Alignment check.
 *
 * SCTLR_EL3.A: Set to zero to disable Alignment fault checking.
 *
 * SCTLR.DSSBS: Set to zero to disable speculation store bypass
 * safe behaviour upon exception entry to EL3. 关闭硬件随机预取
 * -------------------------------------------------------------
 */
 mov_imm x0, (SCTLR_RESET_VAL & ~(SCTLR_EE_BIT | SCTLR_WXN_BIT \
 | SCTLR_SA_BIT | SCTLR_A_BIT | SCTLR_DSSBS_BIT))
 msr sctlr_el3, x0
 isb
 .endif /* _init_sctlr */

.if \_warm_boot_mailbox
 /* -------------------------------------------------------------
 * This code will be executed for both warm and cold resets.
 * Now is the time to distinguish between the two.
 * Query the platform entrypoint address and if it is not zero
 * then it means it is a warm boot so jump to this address.
 * -------------------------------------------------------------
 */
 bl plat_get_my_entrypoint # 平台实现，返回x0，cold时为0，warm时保存程序地址到X0
 cbz x0, do_cold_boot # if x0 = 0, branch to do_cold_boot
 br x0 # 跳转到X0的地址执行, 像是绝对地址跳转，就不返回了。具体warn怎么处理，要参考一下别的厂家

do_cold_boot: # 后面的代码都是cold boot的
 .endif /* _warm_boot_mailbox */

/* ---------------------------------------------------------------------
 * Set the exception vectors. 把异常向量表地址赋给vbar_el3
 * ---------------------------------------------------------------------
 */
 adr x0, \_exception_vectors
 msr vbar_el3, x0
 isb

/* ---------------------------------------------------------------------
 * It is a cold boot.
 * Perform any processor specific actions upon reset e.g. cache, TLB
 * invalidations etc.
 * ---------------------------------------------------------------------
 */
 bl reset_handler

el3_arch_init_common

.if \_secondary_cold_boot
 /* -------------------------------------------------------------
 * Check if this is a primary or secondary CPU cold boot.
 * The primary CPU will set up the platform while the
 * secondaries are placed in a platform-specific state until the
 * primary CPU performs the necessary actions to bring them out
 * of that state and allows entry into the OS.
 * -------------------------------------------------------------
 */
 bl plat_is_my_cpu_primary
 cbnz w0, do_primary_cold_boot

/* This is a cold boot on a secondary CPU */
 bl plat_secondary_cold_boot_setup
 /* plat_secondary_cold_boot_setup() is not supposed to return */
 bl el3_panic

do_primary_cold_boot:
 .endif /* _secondary_cold_boot */

/* ---------------------------------------------------------------------
 * Initialize memory now. Secondary CPU initialization won't get to this
 * point.
 * ---------------------------------------------------------------------
 */

.if \_init_memory
 bl platform_mem_init
 .endif /* _init_memory */

/* ---------------------------------------------------------------------
 * Init C runtime environment:
 * - Zero-initialise the NOBITS sections. There are 2 of them:
 * - the .bss section;
 * - the coherent memory section (if any).
 * - Relocate the data section from ROM to RAM, if required.
 * ---------------------------------------------------------------------
 */
 .if \_init_c_runtime
#if defined(IMAGE_BL31) || (defined(IMAGE_BL2) && BL2_AT_EL3)
 /* -------------------------------------------------------------
 * Invalidate the RW memory used by the BL31 image. This
 * includes the data and NOBITS sections. This is done to
 * safeguard against possible corruption of this memory by
 * dirty cache lines in a system cache as a result of use by
 * an earlier boot loader stage.
 * -------------------------------------------------------------
 */
 adrp x0, __RW_START__
 add x0, x0, :lo12:__RW_START__
 adrp x1, __RW_END__
 add x1, x1, :lo12:__RW_END__
 sub x1, x1, x0
 bl inv_dcache_range
#endif
 adrp x0, __BSS_START__
 add x0, x0, :lo12:__BSS_START__

adrp x1, __BSS_END__
 add x1, x1, :lo12:__BSS_END__
 sub x1, x1, x0
 bl zeromem

#if USE_COHERENT_MEM
 adrp x0, __COHERENT_RAM_START__
 add x0, x0, :lo12:__COHERENT_RAM_START__
 adrp x1, __COHERENT_RAM_END_UNALIGNED__
 add x1, x1, :lo12: __COHERENT_RAM_END_UNALIGNED__
 sub x1, x1, x0
 bl zeromem
#endif

#if defined(IMAGE_BL1) || (defined(IMAGE_BL2) && BL2_IN_XIP_MEM)
 adrp x0, __DATA_RAM_START__
 add x0, x0, :lo12:__DATA_RAM_START__
 adrp x1, __DATA_ROM_START__
 add x1, x1, :lo12:__DATA_ROM_START__
 adrp x2, __DATA_RAM_END__
 add x2, x2, :lo12:__DATA_RAM_END__
 sub x2, x2, x0
 bl memcpy16
#endif
 .endif /* _init_c_runtime */

/* ---------------------------------------------------------------------
 * Use SP_EL0 for the C runtime stack.
 * ---------------------------------------------------------------------
 */
 msr spsel, #0

/* ---------------------------------------------------------------------
 * Allocate a stack whose memory will be marked as Normal-IS-WBWA when
 * the MMU is enabled. There is no risk of reading stale stack memory
 * after enabling the MMU as only the primary CPU is running at the
 * moment.
 * ---------------------------------------------------------------------
 */
 bl plat_set_my_stack

#if STACK_PROTECTOR_ENABLED
 .if \_init_c_runtime
 bl update_stack_protector_canary
 .endif /* _init_c_runtime */
#endif
 .endm

#endif /* EL3_COMMON_MACROS_S */
pl
```

##  冷启动还是热启动

_warm_boot_mailbox执行流程

- 调用平台实现的函数plat_get_my_entrypoint()，判断cold or warm reset
- 若为cold reset，必须返回0，继续执行后面的代码
- 若为warm reset，plat_get_my_entrypoint()返回一个warm reset的执行地址，直接跳过去执行，不再回来。 这个地址与bl31相关

先看warm or cold reset这块，_warm_boot_mailbox参数接收的值为 "!PROGRAMMABLE_RESET_ADDRESS"，PROGRAMMABLE_RESET_ADDRESS是在makefile中定义，所以需要走 el3_entrypoint_common函数下 .if \_warm_boot_mailbox这个分支

该分支探测基于平台入口地址: 如果是0那么是从冷启动，否则是热启动，在后面一种情况下，这个宏跳转到平台入口点地址。

```assembly
 bl plat_get_my_entrypoint 	# 平台实现，返回x0，cold时为0，warm时保存程序地址到X0
 cbz x0, do_cold_boot 		# if x0 = 0, branch to do_cold_boot
 br x0 						# 跳转到X0的地址执行, 像是绝对地址跳转，就不返回了。具体warn怎么处理，要参考一下别的厂家
```

tsingmicro平台的plat_get_my_entrypoint的函数定义,路径：/atf/plat/tsingmicro/common/aarch64/plat_helpers.S

plat_get_my_entrypoint的主要惯例是区分是冷启动还是热启动

在禁用MMU和Cache的情况下调用此函数（SCTLR_EL3.M = 0且SCTLR_EL3.C = 0）。 该函数负责使用各个平台自己的方法区分当前CPU的warm boot和cold boot。 如果是热复位，则它返回在BL31初始化期间提供给plat_setup_psci_ops（）的warm boot入口点。 如果是冷复位，必须返回零。

简单的说，这个函数在cold boot时返回0，warm boot,返回malibox中包含的地址相关信息，这个地址很有可能是**电源状态转换之前设置的入口地址**。各个平台需要实现：

```assembly
	.globl	plat_get_my_entrypoint
	...
		/* ---------------------------------------------------------------------
	 * uintptr_t plat_get_my_entrypoint (void);
	 *
	 * Main job of this routine is to distinguish between a cold and a warm
	 * boot.
	 *
	 * This functions returns:
	 *  - 0 for a cold boot.
	 *  - Any other value for a warm boot.
	 * ---------------------------------------------------------------------
	 */
func plat_get_my_entrypoint
	mov	x1, x30
	bl	plat_is_my_cpu_primary			//查询当前CPU是否是主CPU
	/*
	 * Secondaries always cold boot.
	*/								//如果是w0==0 跳转到1f处
	cbz	w0, 1f						//Compare and branch on zero 有条件相对跳转 不返回 不保存返回地址
	/*
	 * Primaries warm boot if they are requested
	 * to power off.
	 * 
	 * //platform_def.h中定义#define PLAT_TSM_PM_SYS_FLAG_ADDR (TSM_SMU_BASE + TSM_SMU_USR_GPR0_OFF)
	 */
	mov_imm	x0, PLAT_TSM_PM_SYS_FLAG_ADDR	
	ldr	x0, [x0]
	cmp	x0, PLAT_TSM_PM_WARM_BOOT_FLAG		//比较热启动的BOOT FLAG
	//adr	x0, plat_wait_for_warm_boot
	adr	x0, plat_tsm_warm_boot_entry		//热启动的入口
	csel	x0, x0, xzr, eq
	ret	x1
1:	mov	x0, #0
	ret	x1
endfunc plat_get_my_entrypoint
```

plat_is_my_cpu_primary函数的路径:atf/plat/tsingmicro/common/aarch64/plat_helpers.S

该函数查寻当前CPU是否是主CPU

```assembly
	.globl	plat_is_my_cpu_primary
	....
	/* -----------------------------------------------------
	 * unsigned int plat_is_my_cpu_primary (void);
	 *
	 * Find out whether the current cpu is the primary
	 * cpu.
	 * -----------------------------------------------------
	 */
func plat_is_my_cpu_primary
	mrs	x0, mpidr_el1										//move mpidr_el1 into x0
	and	x0, x0, #(MPIDR_CLUSTER_MASK | MPIDR_CPU_MASK)		//x0 = x0 & ((MPIDR_CLUSTER_MASK | MPIDR_CPU_MASK))
	cmp	x0, #TSM_PRIMARY_CPU		//在platform_def.h中定义: #define TSM_PRIMARY_CPU U(0)
	cset	w0, eq					//指令根据当前标志位的值判断指令中的<cond>条件是否成立
	ret
endfunc plat_is_my_cpu_primary
```

这里有一个mpidr_el1寄存器，全称是Multi-Processor Affinity Register。可以让软件洞悉自己在哪个core上执行，无论是在集群内还是在具有多个集群的系统中，它都可以确定它在哪个核心上和在哪个集群中执行

相关的几个宏在atf/include/arch/aarch64/arch.h中定义

```c
#define MPIDR_AFFLVL_MASK	ULL(0xff)
#define MPIDR_AFFINITY_BITS	U(8)
#define MPIDR_CPU_MASK		MPIDR_AFFLVL_MASK
#define MPIDR_CLUSTER_MASK	(MPIDR_AFFLVL_MASK << MPIDR_AFFINITY_BITS)
```

比较x0中获取到的Core ID和platform_def定义的primary cpu是否相等后返回

回到plat_get_my_entrypoint的27行

plat_get_my_entrypoint定义在platform_def.h中

```
#define PLAT_TSM_PM_SYS_FLAG_ADDR (TSM_SMU_BASE + TSM_SMU_USR_GPR0_OFF)	//TSM_SMU_USR_GPR0_OFF=0x100
#define PLAT_TSM_PM_WARM_BOOT_FLAG ULL(0xFC2)
```

![image-20231019140841035](C:\Users\EDZ\Desktop\translate_arm\armv8_programmer_guide_translate\note\ATF 启动流程.assets\image-20231019140841035.png)

SMU中有几个用户定义通用目标寄存器，可以用来存放warm boot的地址

```assembly
func plat_tsm_warm_boot_entry
	/* Jump to the provided entrypoint */
	mov_imm	x0, PLAT_RPI3_TM_ENTRYPOINT
	ldr	x1, [x0]
	br	x1
endfunc plat_tsm_warm_boot_entry
```

```assembly
/*
 * Arm TF lives in SRAM, partition it here
 */
#define SHARED_RAM_SIZE ULL(0x00001000)
#define SHARED_RAM_BASE (SEC_SRAM_BASE + SEC_SRAM_SIZE - SHARED_RAM_SIZE)	//tsingmicro平台目前是以SRAM最后4KB

/* The secure entry point to be used on warm reset by all CPUs. */
#define PLAT_RPI3_TRUSTED_MAILBOX_BASE SHARED_RAM_BASE
#define PLAT_RPI3_TM_ENTRYPOINT PLAT_RPI3_TRUSTED_MAILBOX_BASE
#define PLAT_RPI3_TM_ENTRYPOINT_SIZE ULL(8)
```



在tsm_pm.c中发现有这样一个函数:

看着像是在电源管理模块，进入power down状态（也就是电源状态转换之前）的时候执行该函数

主CPU向PLAT_TSM_PM_SYS_FLAG_ADDR地址上写入像hold_base中写入PLAT_TSM_PM_WARM_BOOT_FLAG，重启时就可以热启动的链路

secondaries的CPU总是 处于等待重置到热启动，但是BSP需要能够区分waiting for warm boot 和 冷启动

```C
#define __dead2		__attribute__((__noreturn__))

static void __dead2 ts_pwr_down_wfi(
		const psci_power_state_t *target_state)
{
	uintptr_t hold_base = PLAT_TSM_PM_SYS_FLAG_ADDR;
	unsigned int pos = plat_my_core_pos();

	if (pos == 0) {	
		/*
		 * The secondaries will always be in a wait
		 * for warm boot on reset, but the BSP needs
		 * to be able to distinguish between waiting
		 * for warm boot (e.g. after psci_off, waiting
		 * for psci_on) and a cold boot.
		 */
		mmio_write_64(hold_base, PLAT_TSM_PM_WARM_BOOT_FLAG);		//向hold_base中写入PLAT_TSM_PM_WARM_BOOT_FLAG
		/* No cache maintenance here, we run with caches off already. */
		dsb();
		isb();
	}
#ifdef TSM_PM_TEST_EN
	//TSM,temp test code
	tsm_test_warm_boot(pos);
#endif

	write_rmr_el3(RMR_EL3_RR_BIT | RMR_EL3_AA64_BIT);	//WFI休眠?

	while (1)
		;
}
```

追溯到该函数的注册位置:

```C
/*******************************************************************************
 * Platform handlers and setup function.
 ******************************************************************************/
static const plat_psci_ops_t plat_ts_psci_pm_ops = {
	.cpu_standby = ts_cpu_standby,
	.pwr_domain_on = ts_pwr_domain_on,
	.pwr_domain_on_finish = ts_pwr_domain_on_finish,
	.pwr_domain_off = ts_pwr_domain_off,
	.pwr_domain_suspend = tsm_pwr_domain_suspend,
	.pwr_domain_suspend_finish = tsm_pwr_domain_suspend_finish,
	.pwr_domain_pwr_down_wfi = ts_pwr_down_wfi,	//在这里
	.system_off = ts_system_off,
	.system_reset = ts_system_reset,
	.validate_power_state = ts_validate_power_state,
	.get_sys_suspend_power_state = tsm_get_sys_suspend_power_state
};

int plat_setup_psci_ops(uintptr_t sec_entrypoint,
			const plat_psci_ops_t **psci_ops)
{
	uintptr_t *entrypoint = (void *) PLAT_RPI3_TM_ENTRYPOINT;

	*entrypoint = sec_entrypoint;
	*psci_ops = &plat_ts_psci_pm_ops;

	return 0;
}

```



测试热启动：

```C
#ifdef TSM_PM_TEST_EN
extern uintptr_t plat_get_my_entrypoint(void);
extern void plat_secondary_cold_boot_setup(void);
static void
tsm_test_warm_boot(int core)
{
	disable_mmu_el3();
	if(core == 0){
		void (*entry)(void);
		entry = (void *)plat_get_my_entrypoint();
		entry();
	}else{
		plat_secondary_cold_boot_setup();
	}
}
#endif
```

整个冷启动or热启动的探测和执行过程:

![image-20231019152017963](C:\Users\EDZ\Desktop\translate_arm\armv8_programmer_guide_translate\note\ATF 启动流程.assets\image-20231019152017963.png)