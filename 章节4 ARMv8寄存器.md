# 章节4 ARMv8寄存器

AArch64执行状态提供31个可在任意时间所有异常等级下访问的64bit通用寄存器。

每个寄存器都是64bit位宽，通常称为寄存器X0-X30。

**Figure 4-1 AArch64 general-purpose registers**

![image-20231013185810441](./章节4 ARMv8寄存器.assets/image-20231013185810441.png)

每个AArch64 64bit的通用寄存器(X0 - X30)也有一个32bit形式(W0 - W30)

**Figure 4-2 64-bit register with W and X access.**

![image-20231013185948684](./章节4 ARMv8寄存器.assets/image-20231013185948684.png)

32位W寄存器构成相应64位X寄存器的低半部分，也就是说，W0映射到X0的低字，而W1映射到X1的低字。(一个字占据32bit宽度)

读取W寄存器会丢掉对应的X寄存器的高32bit并保持他们不变。写入W寄存器会设置高32位的X寄存器为0。例如写0xFFFFFFFF到W0会设置X0为0x00000000FFFFFFFF。

## 4.1 AArch64 special registers

除了31个core registers，也有一些特殊寄存器

**Figure 4-3 AArch64 special registers**

![image-20231019191300601](C:\Users\EDZ\Desktop\translate_arm\armv8_programmer_guide_translate\章节4 ARMv8寄存器.assets\image-20231019191300601.png)

**Note**

不存在叫做X31或W31的寄存器，许多指令被编码为例如数字31表示零寄存器ZR(WZR/XZR)。还有一组受限制的指令，其中一个或多个参数被编码为数字31表示堆栈指针(SP)

访问zero register的时候，所有写操作被忽略，所有读操作返回0。注意64-bit形式的SP寄存器不需要使用X前缀

**Table 4-1 Special registers in AArch64**

![image-20231019192428323](C:\Users\EDZ\Desktop\translate_arm\armv8_programmer_guide_translate\章节4 ARMv8寄存器.assets\image-20231019192428323.png)

在ARMv8体系结构中，当执行在AArch64状态时，异常返回的状态被保存在以下每个异常级别的专用寄存器中:

- Exception Link Register(ELR)
- Saved Processor State Register (SPSR)

每个异常等级有专门的SP，但它并不用来保存返回状态

**Table 4-2 Special registers by Exception level**

![image-20231019192749269](C:\Users\EDZ\Desktop\translate_arm\armv8_programmer_guide_translate\章节4 ARMv8寄存器.assets\image-20231019192749269.png)

### 4.1.1 Zero Register

 Zero Register当作源寄存器用的时候，读出来是0。当作目的寄存器来用的时候，会丢弃结果。您可以在大多数(但不是全部)指令中使用Zero Register

### 4.1.2 Stack pointer

在ARMv8体系结构中，栈指针使用的选择在一定程度上与异常等级分开。默认地，发生一个异常会为目标异常等级选择栈指针，SP_ELn。例如，发生一个异常到EL1会选择SP_EL1，每一个异常登记下都有自己独立的栈指针，SP_EL0，SP_EL1，SP_EL2，SP_EL3。

当在AArch64状态处于除EL0以外的异常等级时，处理器可以使用:

- 与那个异常等级关联的一个专用的64bit栈指针(SP_ELn)
- 与EL0关联的栈指针(SP_EL0)

EL0下只能访问SP_EL0

**Table 4-3 AArch64 Stack pointer options**

![image-20231019193947275](C:\Users\EDZ\Desktop\translate_arm\armv8_programmer_guide_translate\章节4 ARMv8寄存器.assets\image-20231019193947275.png)



后缀“t”表示选择SP_EL0堆栈指针。h后缀表示SP_ELn堆栈指针被选中

SP无法被大部分指令所应用，然而，一些形式的算术指令，如add指令，可以读写当前的栈指针以调整函数中的栈指针。例如：

```
ADD SP, SP, #0x10 // Adjust SP to be 0x10 bytes before its current valu
```



### 4.1.3 Program Counter

源与的ARMv7指令集的一个特色，是使用了R15，也就是程序计数器(PC)作为一个通用目的寄存器。PC实现了一些聪明的编程技巧，但它给编译器和复杂的流水线的设计带来了复杂性。ARMv8中移除了对PC的直接访问让返回预测变得更加容易并且简化了ABI规范。

PC从不作为有名寄存器被访问，它的使用是隐式的以特定的指令例如PC-relative load和address generation。PC不能被当作一个数据处理指令或者load指令的目的寄存器。

### 4.1.4  Exception Link Register (ELR)

Exception Link Register保存着异常的返回地址

### 4.1.5 Saved Process Status Register(SPSR)

发生一个异常时，处理器状态存储在相关的*Saved Process Status Register*(spsr)中，以一种与ARMv7的CPSR相似地方法，在发生异常之前SPSR保存着PSTATE的值且被用于恢复PSTATE当执行异常返回时。

**Figure 4-3 SPSR**

![image-20231019195327704](C:\Users\EDZ\Desktop\translate_arm\armv8_programmer_guide_translate\章节4 ARMv8寄存器.assets\image-20231019195327704.png)

每个位代表着AArch64的以下值：

![image-20231019195537338](C:\Users\EDZ\Desktop\translate_arm\armv8_programmer_guide_translate\章节4 ARMv8寄存器.assets\image-20231019195537338.png)

在ARMv8中，SPSR的写入依赖与异常等级:

- 如果异常发生在EL1中，则使用SPSR_EL1。
- 如果异常发生在EL2中，则使用SPSR_EL2。
- 如果异常发生在EL3中，则使用SPSR_EL3。

core会在异常发生时调节SPSR

**Note**

与Exception级别相关联的寄存器对ELR_ELn和SPSR_ELn在较低的Exception级别执行期间保持其状态

## 4.2 Processor state

AArch64没有一个直接与ARMv7的*Current Program Status Register*  (**CPSR**).相等的寄存器。在AArch64中，传统的CPSR组件作为可独立访问的字段提供。这些统称为处理器状态(**PSTATE**)。

**Table 4-4 PSTATE field definitions**

![image-20231019200215381](C:\Users\EDZ\Desktop\translate_arm\armv8_programmer_guide_translate\章节4 ARMv8寄存器.assets\image-20231019200215381.png)

在AArch64中，您可以通过执行ERET指令来从一个异常返回，这将引起SPSR_ELn被拷贝到PSTATE，恢复ALU标志，执行状态，异常等级，以及处理器分支。从此开始，您可以继续从ELR_ELn开始执行。

PSTATE.{N, Z, C, V} 字段能够被在EL0访问，所有其他的PSTATE字段可以在EL1或者更高等级访问，在EL0下是未定义的。

## 4.3 System registers

在AArch64中,系统配置通过system registers去控制，通过MSR和MRS指令去访问。这与ARMv7-A形成对比，在ARMv7-A中，这样的寄存器通常通过协处理器15 (CP15)操作访问。寄存器的名字告诉你它最低能被从哪个异常等级访问。

例如：

- TTBR0_EL1可以从EL1,EL2,EL3访问

- TTBR0_EL2可以从EL2,EL3访问

具有_ELn后缀的寄存器在某些或所有级别上都有一个单独的存储副本，尽管通常不是EL0。很少有系统寄存器可以从EL0访问，尽管缓存类型
寄存器(CTR_EL0)就是一个可以访问的寄存器的例子。

访问system register的代码使用如下形式:

```
MRS x0, TTBR0_EL1 // Move TTBR0_EL1 into x0
MSR TTBR0_EL1, x0 // Move x0 into TTBR0_EL
```

ARM架构的旧版本使用协处理器来作系统配置，然而，AArch64不包含对协处理器的支持， Table 4-5列出了这本书中提到的系统寄存器。

完整的列表请参见e ARM Architecture Reference Manual - ARMv8, for  ARMv8-A architecture profile的附录J

**Table 4-5 System registers**

![image-20231019201429369](C:\Users\EDZ\Desktop\translate_arm\armv8_programmer_guide_translate\章节4 ARMv8寄存器.assets\image-20231019201429369.png)

![image-20231019201448212](C:\Users\EDZ\Desktop\translate_arm\armv8_programmer_guide_translate\章节4 ARMv8寄存器.assets\image-20231019201448212.png)



![image-20231019201501211](C:\Users\EDZ\Desktop\translate_arm\armv8_programmer_guide_translate\章节4 ARMv8寄存器.assets\image-20231019201501211.png)

![image-20231019201510879](C:\Users\EDZ\Desktop\translate_arm\armv8_programmer_guide_translate\章节4 ARMv8寄存器.assets\image-20231019201510879.png)

![image-20231019201523500](C:\Users\EDZ\Desktop\translate_arm\armv8_programmer_guide_translate\章节4 ARMv8寄存器.assets\image-20231019201523500.png)

### 4.3.1 The system control register

 System Control Register (SCTLR) 是一个控制标准的内存，系统工具，并为实现在核内的功能提供状态信息的一个寄存器

**Figure 4-5 SCTLR bit assignments**

![image-20231019201807834](C:\Users\EDZ\Desktop\translate_arm\armv8_programmer_guide_translate\章节4 ARMv8寄存器.assets\image-20231019201807834.png)

在EL1下不是所有的bit都是可以访问的，单个bit表示以下内容：

![image-20231019201954538](C:\Users\EDZ\Desktop\translate_arm\armv8_programmer_guide_translate\章节4 ARMv8寄存器.assets\image-20231019201954538.png)



![image-20231019202003743](C:\Users\EDZ\Desktop\translate_arm\armv8_programmer_guide_translate\章节4 ARMv8寄存器.assets\image-20231019202003743.png)

**访问SCTLR：**

为了访问SCTLR：

```
MRS <Xt>, SCTLR_ELn // Read SCTLR_ELn into Xt
MSR SCTLR_ELn, <Xt> // Write Xt to SCTLR_ELn
```

例如：

**Example 4-1 Setting bits in the SCTL**

``` 
MRS X0, SCTLR_EL1 // Read System Control Register configuration data
ORR X0, X0, #(1 << 2) // Set [C] bit and enable data caching
ORR X0, X0, #(1 << 12) // Set [I] bit and enable instruction caching
MSR SCTLR_EL1, X0 // Write System Control Register configuration data
```

**Note**

在任意异常等级下的数据缓存和指令缓存被使能之前必须使处理器里的caches失效。

## 4.4 Endianness

有两种查看内存中字节的基本方法，一种是Little-Endian (LE)，另一种是大端法(BE)。在大端机器上，内存中对象的最高有效字节存储在最低地址，即最接近零的地址。在小端机器上，最低有效位字节存储在最低地址。也可以使用术语字节排序而不是端序

**Figure 4-6**

![image-20231019202457597](C:\Users\EDZ\Desktop\translate_arm\armv8_programmer_guide_translate\章节4 ARMv8寄存器.assets\image-20231019202457597.png)

每个异常等级下的数据端序是单独控制的。像EL3, EL2 and  EL1，相关的SCTLR_ELn寄存器设置端序。EL1的额外bitSCTLR_EL1.E0E控制EL0的数据端序设置。在AArch64执行状态下，数据访问可以是LE或BE，而取指总是LE的。

处理器是否同时支持LE和BE取决于处理器的实现。如果只支持little-endianness，则EE和E0E位始终为0。
类似地，如果只支持大端结构，则EE和E0E位处于静态1值

当使用AArch32时，具有CPSR.E位对等效系统有不同的值控制寄存器EE位在EL1、EL2或EL3时现在已弃用。ARMv7的使用SETEND指令也已弃用。通过设置SCTLR，可以在执行SETEND指令时触发Undef异常，通过设置SCTLR.SED位

## 4.5 Changing execution state (again)

在第3-8页的更改执行状态中，我们从异常等级的角度描述了AArch64和AArch32的之间的改变。现在我们从寄存器的角度来考虑这个变化。
从使用AArch32的异常进入使用AArch64的异常:

- 使用AArch32执行时，任何较低的Exception级别都可以访问寄存器的上32位的值是UNKNOWN。
- 在AArch32执行期间不可访问的寄存器保留它们在AArch32执行之前的状态。
- 当EL2使用AArch32时，在异常进入EL3时，ELR_EL2的高32位值是UNKNOWN的。
- 栈指针(sp)和异常链接寄存器(elr)在AArch32执行期间不可访问的异常级别，在该异常级别保留它们在AArch32执行之前的状态。这适用于下列登记册:
  -  — SP_EL0.
  -  — SP_EL1.
  -  — SP_EL2.
  -  — ELR_EL1

一般地，应用程序员写为AArch32或者AArch64编写应用程序，只有操作系统必须考虑这两种执行状态以及他们之间的切换。

### 4.5.1 AArch32状态下的寄存器

与ARMv7几乎相同意味着AArch32必须与ARMv7权限级别匹配。这也意味着AArch32只会处理ARMv7 32bit通用寄存器。因此，在ARMv8架构和AArch32执行状态提供的视角之间，必须有一些对应关系。

请记住，在ARMv7体系结构中有16个32位通用寄存器(R0-R15)给软件使用。他们中的15个(R0-R14)可以被用于通用的数据存储，剩下的寄存器，R15，是程序计数寄存器，它的值随着core的执行指令而改变。软件也能够访问CPSR，并且从上一个执行状态保存的CPSR副本，就是SPSR。在发生异常时，将CPSR复制到接受异常的模式的SPSR。

这些寄存器中的哪个寄存器被访问以及在哪里被访问，依赖于软件正在执行的处理器模式和寄存器本身。这被称为banking，Figure 4-7 中的被掩盖的寄存器是banked的。它们使用物理上不同的存储常常只在处理器执行在一个特定的模式时被访问。

**Figure 4-7 The ARMv7 register set showing banked registers**

![image-20231021213902118](C:\Users\EDZ\Desktop\translate_arm\armv8_programmer_guide_translate\章节4 ARMv8寄存器.assets\image-20231021213902118.png)

Bnank用于ARMv7减少异常的等待时间。然而，这也意味着在相当数量的可能寄存器中，任何时候可以使用的寄存器都不到一半。

相对地，AArch64执行状态下有31个所有时间所有异常等级都可以访问的64bit的通用目的寄存器可访问。在AArch64和AArch32执行状态之间的改变意味着AArch64寄存器必定映射到 AArch32 (ARMv7)寄存器集上。这个映射显示在**Figure 4-8**上

在AArch32执行状态下AArch64的高32bit是无法访问的。如果处理器执行在AArch32状态，它使用32bit的W寄存器，等价于ARMv7的32bit寄存器。

AArch32将存储的寄存器映射到否则将无法访问的AArch64寄存器(TODO:没理解)

**Figure 4-8 AArch64 to AArch32 register mapping**

![image-20231021214728818](C:\Users\EDZ\Desktop\translate_arm\armv8_programmer_guide_translate\章节4 ARMv8寄存器.assets\image-20231021214728818.png)



AArch32内的SPSR和ELR_Hyp寄存器是额外的寄存器，只可以使用系统指令访问。他们没有被映射到AArch64体系结构的通用目的寄存器空间内。这些寄存器中的一些在AArch32和AArch64之间映射：

- SPSR_svc maps to SPSR_EL1.
- SPSR_hyp maps to SPSR_EL2.
- ELR_hyp maps to ELR_EL2

下面的寄存器只能在 AArch32状态下访问，然而，由于EL1状态使用AArch64，他们保留着他们自身的状态，尽管他们在AArch64状态下，在那个异常等级下，是无法访问的。

- SPSR_abt. 
- SPSR_und. 
- SPSR_irq. 
- SPSR_fiq

SPSR寄存器只在AArch64状态期间以更高的异常级别进行上下文切换时才是可访问的。

再一次，如果一个异常是从AArch32的异常级别被带到一个AArch64的异常级别，AArch64的ELR_ELn的高32bit都是0

### AArch32状态下的PSATE

AArch64状态下，传统的CPSR的不同组件作为处理器状态（PSTATE）被展现，可以被独立的访问。在AArch32，有额外的字段对应着ARMv7的CPSR位。

**Figure 4-9 CPSR bit assignments in AArch32**

![image-20231021220432532](C:\Users\EDZ\Desktop\translate_arm\armv8_programmer_guide_translate\章节4 ARMv8寄存器.assets\image-20231021220432532.png)

只能在AArch32上访问的附加PSTATE位：

**Table 4-6 PSTATE bit definitions**

![image-20231021220537204](C:\Users\EDZ\Desktop\translate_arm\armv8_programmer_guide_translate\章节4 ARMv8寄存器.assets\image-20231021220537204.png)

## 4.6 NEON and floating-point registers

除了通用寄存器外，ARMv8还具有32个128位浮点寄存器。标签为V0-V31。这32个寄存器用于保存标量的浮点操作数。浮点指令以及用于NEON运算的标量和向量操作数。NEON和浮点寄存器也包含在 **Chapter 7 AArch64 Floating-point and NEON**

### 4.6.1 Floating-point register organization in AArch64

TODO：