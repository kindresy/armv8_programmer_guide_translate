---

---

# 章节2 ARMv8-A架构和处理器

## ARMv8-A

ARM架构可以追溯到1985年，但它并不是一成不变的，相反，它从早期的ARM核心开始已经有了巨大的发展，每一步都增加了特性和功能:

**ARMv4 and earlier**

​	这些处理器只使用了ARM 32bit指令集

**ARMv4T**

​	ARMv4T架构增加了Thumb 16bit指令集到ARM32bit指令集中，这是第一个获得广泛许可的架构。它由ARM7TDMI和ARM9TDMI处理器实现

**ARMv5TE**

   ARMv5TE架构增加了对dsp类型操作、饱和算法以及ARM和Thumb互连的改进。 ARM926EJ-S®实现了这种架构

**ARMv6**

​	ARMv6做了一些提升，包括对非对齐内存访问的支持，对内存架构重要的改变和对多处理器支持的。此外，还包括对32位寄存器内对字节或半字进行操作的SIMD操作的一些支持。ARM1136JF-S®实现了这种架构。 ARMv6架构也提供了一些可选的扩展，特别是Thumb-2和Security Extensions(TrustZone). Thumb-2扩展了Thumb成为一种混合16bit和32bit的指令集。

**ARMv7-A**

​	ARMv7-A架构强制使用 Thumb-2扩展并增加了高级SIMD扩展(NEON)，在ARMv7之前，所有cores基本上都遵循相同的架构或特性集，为了帮助不同的应用程序寻址更大的范围，ARM引入了一组体系结构概要文件:

- ARMv7-A提供支持例如Linux平台操作操作系统的所有必须的特性

- ARMv7-R提供可预测的real-time high-performance

- ARMv7-M针对深度嵌入式微控制器

   M profile也被增加到ARMv6体系结构以在旧体系结构使能新特性，ARMv6M profile被用于低功耗的低成本微处理器。

**ARMv8-A**

​	ARMv8-A是最新一代的ARM体系结构，针对Applications Profile。ARMv8这个名称被用于描述所有当下包括32位执行和64位执行的整个体系结构。它引入了使用64位宽寄存器执行的能力，同时保留了与现有ARMv7软件的向后兼容性。

<img src="./assets/imag1.png" alt="资源错误" style="zoom:30"/>

ARMv8-A体系结构引入了很多改变，从而能够设计出显著更高性能的处理器实现。

**Large physical address**

​	Large physical address让处理器可以访问超过4GB的物理内存

**64-bit virtual addressing**

​	64-bit virtual addressing让虚拟内存摆脱4GB的限制，这对于使用内存映射文件I/O或稀疏寻址的现代桌面软件和服务器软件十分重要

**Automatic event signaling**

​	实现了高能效、高性能的自旋锁

**Larger register files**

​	31个64bit通用寄存器提升了性能，减少了栈的使用

**Efficient 64-bit immediate generation**

​	对literal pools的需求更少

**Large PC-relative addressing range**

​	A +/- 4GB的寻址范围给共享库和位置无关的可执行文件高效的数据寻址

**Additional 16KB and 64KB translation granules**

​	这降低了*Translation Lookaside Buffer* (TLB)的miss概率和page walks的深度

**New exception model**

​	减少了操作系统(OS)和虚拟机管理程序(hypervisor)的复杂度

**Efficient cache managemen**

​	用户空间的缓存操作提高动态代码生成效率，快速的数据缓存清除 Fast  Data cache clear using a Data Cache Zero instruction.

**Hardware-accelerated cryptography** 

​	软件加密性能提高3到10倍。这对于小粒度解密和加密非常有用，因为它们太小而无法有效地卸载到硬件加速器，例如HTTPS

**Load-Acquire, Store-Release instructions**

​	为C++11, C11, Java没存模型而设计，他们通过消除显式内存屏障指令来提高线程安全代码的性能

**NEON double-precision floating-point advanced SIMD**

​	这使得SIMD向量化应用到广泛的算法集，例如，科学计算，高性能计算(HPC)和超级计算机。



## 2.2 ARMv8-A Processor properties

表格2-1对比了实现支持ARMv8-A体系结构处理器的属性

**Table 2-1 Comparison of ARMv8-A processors**

|                                  | Processor                                       |                                                  |
| -------------------------------- | ----------------------------------------------- | ------------------------------------------------ |
|                                  | Cortex-A53                                      | Cortex-A57                                       |
| Release date                     | July 2014                                       | January 2015                                     |
| Typical clock speed              | 2GHz on 28nm                                    | 1.5 to 2.5 GHz on 20nm                           |
| Execution order                  | In-order                                        | Out of order, speculative  issue, superscalar    |
| Cores                            | 1 to 4                                          | 1 to 4                                           |
| Integer Peak throughput          | 2.3MIPS/MHz                                     | 4.1 to 4.76MIPS/MHza                             |
| Floating-point Unit              | Yes                                             | Yes                                              |
| Half-precision                   | Yes                                             | Yes                                              |
| Hardware Divide                  | Yes                                             | Yes                                              |
| Fused Multiply Accumulate        | Yes                                             | Yes                                              |
| Pipeline stages                  | 8                                               | 15+                                              |
| Return stack entries             | 4                                               | 8                                                |
| Generic Interrupt Controller     | External                                        | External                                         |
| AMBA interface                   | 64-bit I/F AMBA 4 (Supports AMBA 4  and AMBA 5) | 128-bit I/F AMBA 4 (Supports AMBA 4 and  AMBA 5) |
| L1 Cache size (Instruction)      | 8KB to 64 KB                                    | 48KB                                             |
| L1 Cache structure (Instruction) | 2-way set associative                           | 3-way set associative                            |
| L1 Cache size (Data)             | 8KB to 64KB                                     | 32KB                                             |
| L1 Cache structure (Data)        | 4-way set associative                           | 2-way set associative                            |
| L2 Cache                         | Optional                                        | Integrated                                       |
| L2 Cache size                    | 128KB to 2MB                                    | 512KB to 2MB                                     |
| L2 Cache structure               | 16-way set associative                          | 16-way set associative                           |
| Main TLB entries                 | 512                                             | 1024                                             |
| uTLB entries                     | 10                                              | 48 I-size  32D-side                              |



## 2.2.1 ARMv8 processors

这一节描述了实现ARMv8体系结构的各个处理器，每个例子仅仅给出一个一般性的描述。对于每个处理器更多的具体信息，参见Table 2-1

### The Cortex-A53 processor

Cortex-A53处理器是一款中端低功耗处理器，在单个群集中有1到4个内核，每个内核都有一个L1高速缓存子系统、一个可选的集成GICv3/4接口和一个可选的L2高速缓存控制器

Cortex-A53处理器是一个极度高能效的支持32-bit和64-bi代码的高能效处理器. 他拥有比高度成功的Cortex-A7处理器还要显著的更高性能。它能够作为独立的应用程序处理器进行部署，或是成双地和Cortex-A57处理器作为一个大小核配置来获得更优的性能，更优的可伸缩性，更优的能效。

**Figure 2-2 Cortex-A53 processor**

![image-20231012141722209](./章节2 ARMv8-A架构和处理器.assets/image-20231012141722209.png)

Cortex-A53处理器拥有如下特性：

- 顺序执行，8级流水线
- 通过使用分层的时钟门控，电源域，以及高级的保留模式来降低低功耗
- 功率优化后的二级缓存设计可以提供更低的延迟并平衡性能和效率

### The Cortex-A57 processor

**Figure 2-3 Cortex-A57 processor core**

Cortex-A57处理器针对移动和企业计算机应用程序包括计算加强的64-bit应用程序例如高端计算，平板，以及服务器产品。它能够和Cortex-A53处理器一块使用到大小端配置中，来获取高伸缩性的表现和高更高效的能源使用

Cortex-A57处理器具有与其他处理器的高速缓存一致互操作性，包括用于图形处理器计算的ARM Mali系列图形处理器(GPU)，并为高性能企业提供可选的可靠性和可扩展性功能。申请。它提供比ARMv7 Cortex-A15高得多的性能。处理器，具有更高的能效水平。包含加密扩展。与上一代处理器相比，加密算法的性能提高了10倍

![image-20231012142853970](./章节2 ARMv8-A架构和处理器.assets/image-20231012142853970.png)

The Cortex-A57处理器全面地实现了ARMv8-A体系结构. 它使能了一个集群内部的1到4个核之间的多进程的多核操作。，通过AMBA5 CHI or AMBA 4 ACE技术多重相干的SMP集群成为可能. 通过CoreSight技术可以实现调试和跟踪。

 Cortex-A57处理器有以下特性:

- 乱序执行，15+级流水线

- 省点特性，包括分支预测，tag减少以及缓存查找的移植

- 通过复制执行资源来增加峰值指令吞吐量。采用本地化译码、3宽译码带宽的功耗优化指令译码。

- 性能优化的L2缓存设计，集群内的多个核可以同时访问L2

  









